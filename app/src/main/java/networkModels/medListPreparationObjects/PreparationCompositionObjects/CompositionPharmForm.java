package networkModels.medListPreparationObjects.PreparationCompositionObjects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CompositionPharmForm {

    @SerializedName("name")
    @Expose
    public String compositionPharmFormName;
}
