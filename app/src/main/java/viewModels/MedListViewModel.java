package viewModels;


import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import network.ApiClient;
import networkModels.MedListMainObject;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MedListViewModel extends ViewModel {

    private MutableLiveData<MedListMainObject> preparationsList = new MutableLiveData<>();

    public LiveData<MedListMainObject> getPreparationsData() {
        return preparationsList;
    }

    private MutableLiveData<MedListMainObject> preparationsBySearch = new MutableLiveData<>();

    public LiveData<MedListMainObject> getPreparationBySearchData() {
        return preparationsBySearch;
    }

    public void getPreparationsList() {
        ApiClient.getClient().getMainObjectOfPreparationsList().enqueue(new Callback<MedListMainObject>() {
            @Override
            public void onResponse(Call<MedListMainObject> call, Response<MedListMainObject> response) {
                if (response.isSuccessful() && response.body() != null) {
                    preparationsList.postValue(response.body());
                } else {
                    preparationsList.postValue(null);
                }
            }

            @Override
            public void onFailure(Call<MedListMainObject> call, Throwable t) {
                preparationsList.postValue(null);
            }
        });
    }

    public void getPreparationBySearch(String prepName) {
        ApiClient.getClient().getPreparationBySearch(prepName).enqueue(new Callback<MedListMainObject>() {
            @Override
            public void onResponse(Call<MedListMainObject> call, Response<MedListMainObject> response) {
                if (response.isSuccessful() && response.body() != null) {
                    preparationsBySearch.postValue(response.body());
                } else {
                    preparationsBySearch.postValue(null);
                }
            }

            @Override
            public void onFailure(Call<MedListMainObject> call, Throwable t) {
                preparationsBySearch.postValue(null);
            }
        });
    }

}
