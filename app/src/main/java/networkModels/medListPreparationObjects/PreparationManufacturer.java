package networkModels.medListPreparationObjects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PreparationManufacturer {

    @SerializedName("name")
    @Expose
    public String manufacturerName;

}
