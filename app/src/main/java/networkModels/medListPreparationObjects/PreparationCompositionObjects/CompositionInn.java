package networkModels.medListPreparationObjects.PreparationCompositionObjects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CompositionInn {

    @SerializedName("name")
    @Expose
    public String compositionInnName;
}
