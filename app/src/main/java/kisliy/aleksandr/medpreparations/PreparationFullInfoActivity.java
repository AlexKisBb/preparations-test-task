package kisliy.aleksandr.medpreparations;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import kisliy.aleksandr.medpreparations.databinding.ActivityPreparationFullInfoBinding;
import networkModels.MedListMainObject;
import networkModels.MedListObjectPreparation;
import viewModels.PreparationFullInfoModel;

public class PreparationFullInfoActivity extends AppCompatActivity {
    ActivityPreparationFullInfoBinding binding;
    String preparationId;

    private PreparationFullInfoModel preparationFullInfoModel;
    private MedListObjectPreparation medListObjectPreparation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_preparation_full_info);

        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        onNavBackPressing();

        preparationId = getIntent().getStringExtra("EXTRA_PREPARATION_ID");
        preparationFullInfoModel = ViewModelProviders.of(this).get(PreparationFullInfoModel.class);
        preparationFullInfoModel.getPreparationFullInfo(preparationId);

        observeFullInfoData();
    }

    private void observeFullInfoData() {
        preparationFullInfoModel.getPreparationFullInfoData().observe(this, new Observer<MedListObjectPreparation>() {
            @Override
            public void onChanged(MedListObjectPreparation medListObjectPreparationObserv) {
                if (medListObjectPreparationObserv != null) {
                    binding.setPreparation(medListObjectPreparationObserv);
                } else {
                    toastMsgLg(getResources().getString(R.string.toast_error));
                }

            }
        });
    }

    private void onNavBackPressing() {
        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void toastMsgLg(String msg) {
        Toast toast = Toast.makeText(getApplicationContext(),
                msg, Toast.LENGTH_LONG);
        toast.show();
    }

}
