package adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;


import java.util.ArrayList;

import kisliy.aleksandr.medpreparations.PreparationFullInfoActivity;
import kisliy.aleksandr.medpreparations.R;
import networkModels.MedListObjectPreparation;

public class MedListAdapter extends RecyclerView.Adapter<MedListAdapter.ViewHolder> implements View.OnClickListener {
    private ArrayList<MedListObjectPreparation> arrayPreparations;
    private MutableLiveData<String> preparationId = new MutableLiveData<>();

    public LiveData<String> getPreparationId() {
        return preparationId;
    }

    @Override
    public void onClick(View v) {
        preparationId.postValue(v.getTag().toString());
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private FrameLayout cardFrame;
        public TextView preparationTradeLabel;
        public TextView preparationManufacturerName;

        public ViewHolder(CardView v) {
            super(v);
            preparationTradeLabel = v.findViewById(R.id.preparation_trade_label);
            preparationManufacturerName = v.findViewById(R.id.preparation_manufacturer_name);
            cardFrame = v;
        }
    }

    public MedListAdapter(ArrayList<MedListObjectPreparation> arrayPreparationsConsrtuctor) {
        arrayPreparations = arrayPreparationsConsrtuctor;
    }

    @Override
    public MedListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                        int viewType) {
        CardView v = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_recycler_preparation, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.cardFrame.setTag(arrayPreparations.get(position).id);
        try {
            holder.preparationTradeLabel.setText(arrayPreparations.get(position).preparationTradeLabel.tradeLableName);
        } catch (Exception e) {
            holder.preparationTradeLabel.setText("Trade labe not defined");
        }
        try {
            holder.preparationManufacturerName.setText(arrayPreparations.get(position).preparationManufacturer.manufacturerName);
        } catch (Exception e) {
            holder.preparationManufacturerName.setText("Manufacturer name not defined");
        }
        holder.cardFrame.setOnClickListener(this);
    }

    @Override
    public int getItemCount() {
        return arrayPreparations.size();
    }


}
