package networkModels.medListPreparationObjects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PreparationPackagingDescription {

    @SerializedName("description")
    @Expose
    public String packagingDescription;
}
