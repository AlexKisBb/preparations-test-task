package network;

import networkModels.MedListMainObject;
import networkModels.MedListObjectPreparation;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Api {

    @GET("medicine")
    Call<MedListMainObject> getMedListApiReq(
    );

    @GET("medicine/{id}")
    Call<MedListObjectPreparation> getPreparationFullInfoApiReq(
            @Path("id") String id
    );

    @GET("medicine/")
    Call<MedListMainObject> getPreparationBySearchApiReq(
            @Query("search") String prepName
    );
}
