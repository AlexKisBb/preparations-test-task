package network;

import networkModels.MedListMainObject;
import networkModels.MedListObjectPreparation;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    private static final String BASE_URL = "https://api.pills-prod.sdh.com.ua/v1/";
    private Api apiRequests;
    private static ApiClient getClient = null;
    private Retrofit retrofit;

    private ApiClient(){
        // Логирование для всех запросов к сети
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient networkClient = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build();

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(networkClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        this.apiRequests=retrofit.create(Api.class);
    }

    public static ApiClient getClient() {
        if (getClient == null) {
            getClient = new ApiClient();
        }
        return getClient;
    }

    public Call<MedListMainObject> getMainObjectOfPreparationsList (){
        return apiRequests.getMedListApiReq();
    }

    public Call<MedListObjectPreparation> getFullInfoOfPreparation (String id){
        return apiRequests.getPreparationFullInfoApiReq(id);
    }

    public Call<MedListMainObject> getPreparationBySearch (String prepName){
        return apiRequests.getPreparationBySearchApiReq(prepName);
    }

}
