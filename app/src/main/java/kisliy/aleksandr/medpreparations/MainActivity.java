package kisliy.aleksandr.medpreparations;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.SearchView;
import android.widget.Toast;

import java.util.ArrayList;

import adapters.MedListAdapter;
import kisliy.aleksandr.medpreparations.databinding.ActivityMainBinding;
import networkModels.MedListMainObject;
import networkModels.MedListObjectPreparation;
import viewModels.MedListViewModel;

public class MainActivity extends AppCompatActivity {
    ActivityMainBinding binding;
    private MedListViewModel medListModel;
    private ArrayList<MedListObjectPreparation> preparationsArray = new ArrayList<>();
    private MedListAdapter medListAdapter;
    private RecyclerView.LayoutManager medListLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        recyclerInitMedListAdapter();

        medListModel = ViewModelProviders.of(this).get(MedListViewModel.class);
        medListModel.getPreparationsList();

        observeMedListData();
        observeIntentFromMedList();
        observeSearchReqData();
        searchViewListener();
    }

    private void observeMedListData() {
        medListModel.getPreparationsData().observe(this, new Observer<MedListMainObject>() {
            @Override
            public void onChanged(MedListMainObject medListMainObject) {
                if (medListMainObject != null) {
                    preparationsArray.addAll(medListMainObject.preparationsArray);
                    Log.e("Лист", String.valueOf(preparationsArray.size()));
                    medListAdapter.notifyDataSetChanged();
                } else {
                    toastMsgLg(getResources().getString(R.string.toast_error));
                }
                binding.progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void observeSearchReqData() {
        medListModel.getPreparationBySearchData().observe(this, new Observer<MedListMainObject>() {
            @Override
            public void onChanged(MedListMainObject medListMainObject) {
                if (medListMainObject != null) {
                    preparationsArray.clear();
                    preparationsArray.addAll(medListMainObject.preparationsArray);
                    medListAdapter.notifyDataSetChanged();
                    if (preparationsArray.size() == 0) {
                        binding.searchViewPlaceholder.setVisibility(View.VISIBLE);
                    } else {
                        binding.searchViewPlaceholder.setVisibility(View.GONE);
                    }
                } else {
                    toastMsgLg(getResources().getString(R.string.toast_error));
                }
                binding.progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void observeIntentFromMedList() {
        medListAdapter.getPreparationId().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String id) {
                if (id != null) {
                    startActivity((new Intent(getApplicationContext(), PreparationFullInfoActivity.class)
                            .putExtra("EXTRA_PREPARATION_ID", id)).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                } else {
                    toastMsgLg(getResources().getString(R.string.toast_error));
                }
                binding.progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void recyclerInitMedListAdapter() {
        medListLayoutManager = new LinearLayoutManager(this);
        binding.medListRecyclerView.setLayoutManager(medListLayoutManager);
        medListAdapter = new MedListAdapter(preparationsArray);
        binding.medListRecyclerView.setAdapter(medListAdapter);
    }

    private void searchViewListener() {
        binding.searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                binding.progressBar.setVisibility(View.VISIBLE);
                medListModel.getPreparationBySearch(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
        binding.searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                preparationsArray.clear();
                binding.progressBar.setVisibility(View.VISIBLE);
                medListModel.getPreparationsList();
                binding.searchViewPlaceholder.setVisibility(View.GONE);
                return false;
            }
        });
    }

    private void toastMsgLg(String msg) {
        Toast toast = Toast.makeText(getApplicationContext(),
                msg, Toast.LENGTH_LONG);
        toast.show();
    }
}
