package networkModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import networkModels.medListPreparationObjects.PreparationComposition;
import networkModels.medListPreparationObjects.PreparationManufacturer;
import networkModels.medListPreparationObjects.PreparationPackagingDescription;
import networkModels.medListPreparationObjects.PreparationTradeLabel;

public class MedListObjectPreparation {

    @SerializedName("id")
    @Expose
    public String id;

    @SerializedName("trade_label")
    @Expose
    public PreparationTradeLabel preparationTradeLabel;

    @SerializedName("manufacturer")
    @Expose
    public PreparationManufacturer preparationManufacturer;

    @SerializedName("packaging")
    @Expose
    public PreparationPackagingDescription preparationPackagingDescription;

    @SerializedName("composition")
    @Expose
    public PreparationComposition preparationComposition;
}
