package networkModels.medListPreparationObjects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import networkModels.medListPreparationObjects.PreparationCompositionObjects.CompositionInn;
import networkModels.medListPreparationObjects.PreparationCompositionObjects.CompositionPharmForm;

public class PreparationComposition {

    @SerializedName("description")
    @Expose
    public String compositionDescription;

    @SerializedName("inn")
    @Expose
    public CompositionInn compositionInn;

    @SerializedName("pharm_form")
    @Expose
    public CompositionPharmForm compositionPharmForm;
}
