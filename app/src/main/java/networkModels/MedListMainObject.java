package networkModels;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MedListMainObject {

    @SerializedName("next")
    @Expose
    public String nextPage;

    @SerializedName("results")
    @Expose
    public ArrayList<MedListObjectPreparation> preparationsArray;
}
