package viewModels;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import network.ApiClient;
import networkModels.MedListObjectPreparation;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PreparationFullInfoModel extends ViewModel {
    private MutableLiveData<MedListObjectPreparation> preparationFullInfo = new MutableLiveData<>();

    public LiveData<MedListObjectPreparation> getPreparationFullInfoData() {
        return preparationFullInfo;
    }

    public void getPreparationFullInfo(String preparationId) {
        ApiClient.getClient().getFullInfoOfPreparation(preparationId).enqueue(new Callback<MedListObjectPreparation>() {
            @Override
            public void onResponse(Call<MedListObjectPreparation> call, Response<MedListObjectPreparation> response) {
                if (response.isSuccessful() && response.body() != null) {
                    preparationFullInfo.postValue(response.body());
                } else {
                    preparationFullInfo.postValue(null);
                }
            }

            @Override
            public void onFailure(Call<MedListObjectPreparation> call, Throwable t) {
                preparationFullInfo.postValue(null);
            }
        });
    }
}
